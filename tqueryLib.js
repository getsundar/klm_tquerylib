(function(window) {
    // You can enable the strict mode commenting the following line  
    // 'use strict';

    // This function will contain all our code
    function myLibrary() {
        var _myLibraryObject = {};

        _myLibraryObject.reverse = function() {
                for (var i = 0; i < this.convertedArray.length; i++) {
                    this.convertedArray[i] = this.convertedArray[i].split("").reverse().join("");
                }
                this.arrayString = this.convertedArray.toString();
                return this;
            }
            // Just create a property to our library object.
        _myLibraryObject.tQuery = function(stringToSplit) {
            this.arrayString = stringToSplit;
            this.convertedArray = [];
            return this;
        };
        _myLibraryObject.split = function(delimiter) {
            this.convertedArray = this.arrayString.split(delimiter);
            this.arrayString = this.convertedArray.toString();
            if (this.arrayString.indexOf(",") != -1) {
                this.convertedArray = this.arrayString.split(',');
                this.arrayString = this.convertedArray.toString();
            }
            return this;
        }
        _myLibraryObject.join = function(delimiter) {
            this.convertedArray = this.convertedArray.join(delimiter);
            return this;
        }

        _myLibraryObject.get = function() {
            return this.convertedArray;
        }

        return _myLibraryObject;
    }

    // We need that our library is globally accesible, then we save in the window
    if (typeof(window.tQueryLib) === 'undefined') {
        window.tQueryLib = myLibrary();
    }
})(window);
